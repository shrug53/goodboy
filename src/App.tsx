import Footer from './Components/Footer/Footer';
import { Container } from './App.styled';
import MainContent from './Components/MainContent/MainContent';

function App() {
   return (
      <Container>
         <header></header>
         <MainContent/>
         <Footer />
      </Container>
   ); 
}

export default App;
