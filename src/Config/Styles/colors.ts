const Colors = {
   backButton: '#F3E2D9',
   brownLight: '#CD8B65',
   brownGradient: 'linear-gradient(180deg, #CD8B65 0%, #BB6B3D 100%)',
   brownCurrent: 'linear-gradient(94deg, #000000 0%, #CD8B65 100%)',
   darkGrey: '#585757',
   darkerGrey: '#2F2F2F',
   disableButtonGrey: '#9F9F9F',
   lightGrey: '#FAF9F9',
};

export default Colors;
