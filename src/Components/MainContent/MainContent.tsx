import doggoImg from 'assets/doggoImg.jpg';
import styled from 'styled-components';
import Form from './Form/Form';

function MainContent() {
   return (
      <Container>
         <Form />
         <StyledDoggoImg src={doggoImg} alt='doggoImg' />
      </Container>
   );
}
const StyledDoggoImg = styled('img')`
   height: 730px;
   width: 364px;
   margin-left: 65px;
`;
const Container = styled('div')`
   height: 100%;
   display: flex;
   justify-content: center;
   margin-top: 130px;
`;

export default MainContent;
