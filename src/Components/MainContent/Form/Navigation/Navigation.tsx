import Colors from 'Config/Styles/colors';
import styled from 'styled-components';

function Navigation() {
   return (
      <Container>
         <Normal />
         <Current />
         <Normal />
      </Container>
   );
}

const Normal = styled.div`
   width: 20.6px;
   height: 6px;
   background-color: ${Colors.disableButtonGrey};
   border-radius: 10px;
`;

const Current = styled.div`
   width: 43.77px;
   height: 6px;
   background-color: ${Colors.brownLight};
   border-radius: 10px;
`;

const Container = styled.div`
   display: flex;
   gap: 5px;
   margin: 0 0 28px 0%;
`;

export default Navigation;
