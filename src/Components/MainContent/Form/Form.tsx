import styled from 'styled-components';
import AmountList from './Amount/AmountList';
import FoundationOrShelter from './FoundationOrShelter/FoundationOrShelter';
import Navigation from './Navigation/Navigation';
import NextBackButton from './NextBackButton/NextBackButton';
import ShelterDroplist from './ShelterDropDown';

function Form() {
   return (
      <FormContainer>
         <Navigation />
         <FoundationOrShelter />
         <Title>Vyberte si možnosť, ako chcete pomôcť</Title>
         <ShelterDroplist />
         <AmountList />
         <NextBackButton color='black' />
      </FormContainer>
   );
}

const FormContainer = styled.form`
   display: flex;
   flex-direction: column;
   width: 557px;
`;
const Title = styled.h1`
   font-family: 'Hind', sans-serif;
   font-weight: 700;
   font-size: 46px;
   line-height: 52px;
   margin: 0;
   margin-bottom: 28px;
`;

export default Form;
