import styled from 'styled-components';
import Amount from './Amount';
import EnterAmount from './EnterAmount';

function AmountList() {
   return (
      <Container>
         <RowTitle>Suma ktorou chcete prispiet</RowTitle>
         <Wrapper>
            <Amount number={5} />
            <Amount number={10} />
            <Amount number={20} />
            <Amount number={30} />
            <Amount number={50} />
            <Amount number={100} />
            <EnterAmount />
         </Wrapper>
      </Container>
   );
}
export const Container = styled.div`
   display: flex;
   flex-direction: column;
   margin-bottom: 72px;
`;
export const Wrapper = styled.div`
   display: flex;
`;
export const RowTitle = styled.div`
   font-weight: 800;
   font-size: 16px;
   margin-bottom: 15px;
   margin-left: 5px;
`;

export default AmountList;
