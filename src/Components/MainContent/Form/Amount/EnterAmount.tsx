import styled from 'styled-components';

function Amount() {
   return (
      <Container>
         <Number>
            <Input type='text' /> €
         </Number>
      </Container>
   );
}
export const Container = styled.div`
   display: flex;
   border: 1px solid #dfdfdf;
   border-radius: 8px;
   margin-right: 7px;
   white-space: nowrap;
`;

export const Number = styled.div`
   font-weight: 800;
   padding: 16px;
   font-size: 16px;
`;

export const Input = styled.input`
   border-style: none;
   width: 30px;
   border-bottom: 1px solid #c9c9c9;
   font-weight: 800;
   &:focus {
      outline: none;
   }
`;
export default Amount;
