import styled from 'styled-components';

interface Props {
   number: number;
}
function Amount({ number }: Props) {
   return (
      <Container>
         <Number>{number} €</Number>
      </Container>
   );
}
export const Container = styled.div`
   display: flex;
   border: 1px solid #dfdfdf;
   border-radius: 8px;
   margin-right: 7px;
   white-space: nowrap;
`;

export const Number = styled.div`
   font-weight: 800;
   padding: 16px;
   font-size: 16px;
`;

export const Input = styled.input`
   border-style: none;
`;
export default Amount;
