import { ReactComponent as Icon } from 'assets/dropdownicon.svg';
import colors from 'Config/Styles/colors';
import styled from 'styled-components';

function Shelter() {
   return (
      <Container>
         <WrapperTitle>
            <RowTitle>O projekte</RowTitle>
            <Optional>Nepovinne</Optional>
         </WrapperTitle>
         <DropDown>
            <TextWrapper>
               <RowTitle>O projekte</RowTitle>
               <PlaceHolder>Vyberte si utulok</PlaceHolder>
            </TextWrapper>
            <DropDownIcon />
         </DropDown>
      </Container>
   );
}

const Container = styled.div`
   display: flex;
   flex-direction: column;
   margin-bottom: 40px;
`;
const DropDown = styled.div`
   align-items: center;
   border-radius: 8px;
   border: 1px solid #dfdfdf;
   display: flex;
   justify-content: space-between;
   height: 74px;
   width: 557px;
`;
const DropDownIcon = styled(Icon)`
   margin-right: 27px;
`;
const Optional = styled.div`
   color: ${colors.darkerGrey};
   font-size: 14px;
   font-weight: 800;
`;
const PlaceHolder = styled.div`
   color: ${colors.disableButtonGrey};
   font-size: 16px;
   line-height: 21px;
`;
const RowTitle = styled.div`
   font-weight: 800;
   font-size: 16px;
   line-height: 21px;
`;
const TextWrapper = styled.div`
   display: flex;
   flex-direction: column;
   justify-content: center;
   margin-left: 24px;
`;
const WrapperTitle = styled.div`
   display: flex;
   justify-content: space-between;
   margin-bottom: 15px;
`;

export default Shelter;
