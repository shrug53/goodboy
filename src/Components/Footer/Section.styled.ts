import styled from 'styled-components';
import colors from 'Config/Styles/colors'

export const Header = styled('h3')`
   color: ${colors.darkerGrey};
   font-weight: bold;
   font-size: 16px;
   margin: 0;
`;

export const Paragraph = styled('p')`
   color: ${colors.darkGrey};
   line-height: 32px;
   margin: 0;
`;

export const Wrapper = styled('div')`
   display: flex;
   flex-direction: column;
   width: 170px;
`;
