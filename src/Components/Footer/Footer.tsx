import { Grid, Container, Logo } from './Footer.styled';
import Section from './Section';

function Footer() {
   return (
      <Container>
         <Logo/>
         <Grid>
            <Section header='Nadacia Good Boy'>
               O projekte Ako na to Kontakt
            </Section>
            <Section header='Nadacia Good Boy'>
               Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus
               in interdum ipsum, sit amet.
            </Section>
            <Section header='Nadacia Good Boy'>
               Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus
               in interdum ipsum, sit amet.
            </Section>
         </Grid>
      </Container>
   );
}

export default Footer;
