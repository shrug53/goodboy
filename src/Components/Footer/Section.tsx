import React from 'react';

import { Header, Paragraph,Wrapper } from './Section.styled';

interface Props {
   header: string;
   children: React.ReactNode;
}

function Section({ header, children }: Props) {
   return (
      <Wrapper>
         <Header>{header}</Header>
         <Paragraph>{children}</Paragraph>
      </Wrapper>
   );
}

export default Section;
