import styled from 'styled-components';

import { ReactComponent as FooterLogo } from 'assets/footerLogo.svg';

export const Container = styled('div')`
   display: flex;
   justify-content: center;
`;
export const Logo = styled(FooterLogo)`
   align-self: flex-start;
   margin-right: 168px;
   margin-top: -20px;
`;
export const Grid = styled('footer')`
   display: grid;
   margin-top: auto;
   grid-template-columns: 1fr 1fr 1fr;
   gap: 120px;
   margin-bottom: 131px;
   width: max-content;
`;
